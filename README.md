# README #

Recently I've noticed that most of the time my computer works in idle just playing internet radio. I've decided to prepare a kind of hardware internet radio player. First idea was to use Raspberry Pi and Python script to run the things. So how I decided is how I did it :)

Actually the idea was to minimize dependencies so the only dependency is to have working MPD on target and Python 3 of course.

Despite the fact that project provides HTTP server to configure, change stations and play/stop selected station, no separate www server is used! Project uses native Python http server thingies so no special modules nor plugins to Python are needed.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact