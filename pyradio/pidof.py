import subprocess


def pidof(name):
    p = subprocess.Popen(['pidof', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    output = out.decode()
    return output.split()
