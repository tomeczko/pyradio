
function reloadStatus(){
    $("#statusDiv").load("getStatus");
}

function startRefreshLoop() {
    reloadStatus();
    myInterval = setInterval( "reloadStatus()", 1000 );  // run
}

function makeAction(action) {
    $("#resultDiv").load("?action=" + action, function() {
        reloadStatus();
    });
}

function makeActionPlayStation(stationId) {
    $("#resultDiv").load("?action=play&stationId=" + stationId, function() {
        reloadStatus();
    });
}

function updateResult(content) {
    $("#resultDiv").html(content);
}
