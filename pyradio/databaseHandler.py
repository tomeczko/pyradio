import sqlite3
import time
from queue import Queue
from queue import Empty
from threading import Thread

class SqliteDbWrapper():

    def __init__(self, dbFilename):
        self.dbConn = sqlite3.connect(dbFilename)


    def checkDb(self):
        print("Checking existance of 'stations' table...")
        c = self.dbConn.cursor()
        try:
            c.execute("select count(id) from stations",())
            retVal = c.fetchone()
            data = retVal[0]
        except sqlite3.OperationalError as ex:
            print("There was a SQLite error. Is the database file healthy?", str(ex))
            return False
        return True


    def getNumberOfStations(self):
        c = self.dbConn.cursor()
        c.execute("select count(id) from stations",())
        retVal = c.fetchone()
        return retVal[0]


    def getStationById(self, stationId):
        c = self.dbConn.cursor()
        c.execute("select id, name, orderId, comment from stations where id=?", (stationId, ))
        stationEntry = c.fetchone()
        if stationEntry == None:
            return None
        station = {
            "id": stationEntry[0],
            "name": stationEntry[1],
            "orderId": stationEntry[2],
            "comment": stationEntry[3],
            "urls": []
        }
        for url in c.execute("select id, stationId, inUse, url, comments from urls where stationId=? and inUse=1", (stationId, )):
            station["urls"].append(url[3])
        return station


    def getListOfStations(self):
        c = self.dbConn.cursor()
        listOfStations = []
        for stationEntry in c.execute("select id, name, orderId, comment from stations order by orderId, id", ()):
            station = {
                "id": stationEntry[0],
                "name": stationEntry[1],
                "orderId": stationEntry[2],
                "comment": stationEntry[3]
            }
            listOfStations.append(station)
        return listOfStations


    def getFirstStationId(self):
        c = self.dbConn.cursor()
        c.execute("select id, orderId from stations order by orderId, name limit 1", ())
        stationId = c.fetchone()
        if stationId is None:
            return None
        else:
            return stationId[0]


    def getStationNext(self, stationId):
        currentStationDef = self.getStationById(stationId)
        nextC = self.dbConn.cursor()
        nextC.execute("select id, orderId from stations where orderId=? and id>?",
        (currentStationDef["orderId"], stationId, ))
        stationEntry = nextC.fetchone()
        sameOrderIdExist = (stationEntry != None)
        c = self.dbConn.cursor()
        if sameOrderIdExist:
            c.execute("select id, orderId from stations where orderId=? and id>? order by orderId asc, id asc",
            (currentStationDef["orderId"], stationId, ))
        else:
            c.execute("select id, orderId from stations where orderId>? order by orderId, id asc",
            (currentStationDef["orderId"], ))
        stationEntry = c.fetchone()
        if stationEntry != None:
            print("Next id is", stationEntry[0])
            return self.getStationById(stationEntry[0])
        else:
            print("This was the last matching station")
            c.execute("select id, orderId from stations where id=?", (stationId, ))
            stationEntry = c.fetchone()
            return self.getStationById(stationEntry[0])


    def getStationPrevious(self, stationId):
        currentStationDef = self.getStationById(stationId)
        prevC = self.dbConn.cursor()
        prevC.execute("select id, orderId from stations where orderId=? and id<?",
        (currentStationDef["orderId"], stationId, ))
        stationEntry = prevC.fetchone()
        sameOrderIdExist = (stationEntry != None)
        c = self.dbConn.cursor()
        if sameOrderIdExist:
            c.execute("select id, orderId from stations where orderId=? and id<? order by orderId desc, id desc",
            (currentStationDef["orderId"], stationId, ))
        else:
            c.execute("select id, orderId from stations where orderId<? order by orderId desc, id desc",
            (currentStationDef["orderId"], ))
        stationEntry = c.fetchone()
        if stationEntry != None:
            print("Previous id is", stationEntry[0])
            return self.getStationById(stationEntry[0])
        else:
            print("This was the first matching station")
            c.execute("select id, orderId from stations where id=?", (stationId, ))
            stationEntry = c.fetchone()
            return self.getStationById(stationEntry[0])


    def updateStation(self, stationId, name, orderId, comment):
        if len(name) == 0:
            return(RuntimeError("Name cannot be empty!"))
        c = self.dbConn.cursor()
        c.execute("update stations set name=?, orderId=?, comment=? where id=?",
            (name, orderId, comment, stationId,))
        self.dbConn.commit()
        return True


    def addStation(self, name, orderId, comment):
        if len(name) == 0:
            return(RuntimeError("Name cannot be empty!"))
        c = self.dbConn.cursor()
        c.execute("insert into stations (name, orderId, comment) values (?,?,?)",
            (name, orderId, comment,))
        self.dbConn.commit()
        return c.lastrowid


    def delStation(self, stationId):
        c = self.dbConn.cursor()
        c.execute("delete from urls where stationId=?", (stationId,))
        c.execute("delete from stations where id=?", (stationId,))
        self.dbConn.commit()
        return True


    def updateUrls(self, stationId, urls):
        # Step 1: Remove removed
        c = self.dbConn.cursor()
        entries = c.execute("select id, stationId, inUse, url, comments from urls where stationId=?", (stationId, ))
        for entry in entries:
            if entry[3] not in urls:
                dc = self.dbConn.cursor()
                dc.execute("delete from urls where id=?", (entry[0], ))

        # Step 2: Add added
        for url in urls:
            c = self.dbConn.cursor()
            c.execute("select id, stationId, inUse, url, comments from urls where stationId=? and url=?", (stationId, url, ))
            entry = c.fetchone()
            if entry == None:
                # print("Adding url:", url)
                c.execute("insert into urls (stationId, inUse, url) values(?,?,?)", (stationId, 1, url, ))
        self.dbConn.commit()
        return None


class DatabaseHandler(Thread):

    def __init__(self, databaseFilename):
        self.shutdownVar = False
        self.inQueue = Queue()
        self.outQueue = Queue()
        self.databaseFilename = databaseFilename
        Thread.__init__(self)


    def shutdown(self):
        self.shutdownVar = True


    def run(self):
        print("Starting database thread...")
        dbWrapper = SqliteDbWrapper(self.databaseFilename)
        while self.shutdownVar == False:
            try:
                request = self.inQueue.get(timeout=0.1)
            except Empty:
                continue

            if request["action"] == "checkDb":
                checkResult = dbWrapper.checkDb()
                self.outQueue.put(checkResult)

            if request["action"] == "getNumberOfStations":
                numberOfStations = dbWrapper.getNumberOfStations()
                self.outQueue.put(numberOfStations)

            if request["action"] == "getListOfStations":
                listOfStations = dbWrapper.getListOfStations()
                self.outQueue.put(listOfStations)

            if request["action"] == "getFirstStationId":
                firstStationId = dbWrapper.getFirstStationId()
                self.outQueue.put(firstStationId)

            if request["action"] == "getStationById":
                station = dbWrapper.getStationById(request["id"])
                self.outQueue.put(station)

            if request["action"] == "getStationPrevious":
                station = dbWrapper.getStationPrevious(request["id"])
                self.outQueue.put(station)

            if request["action"] == "getStationNext":
                station = dbWrapper.getStationNext(request["id"])
                self.outQueue.put(station)

            if request["action"] == "updateStation":
                result = dbWrapper.updateStation(
                    request["stationId"],
                    request["name"],
                    request["orderId"],
                    request["comment"])
                self.outQueue.put(result)

            if request["action"] == "addStation":
                result = dbWrapper.addStation(
                    request["name"],
                    request["orderId"],
                    request["comment"])
                self.outQueue.put(result)

            if request["action"] == "delStation":
                result = dbWrapper.delStation(
                    request["stationId"])
                self.outQueue.put(result)

            if request["action"] == "updateUrls":
                result = dbWrapper.updateUrls(
                    request["stationId"],
                    request["urls"])
                self.outQueue.put(result)

        print("Shutting down database handler thread...")


    def checkDb(self):
        self.inQueue.put({"action": "checkDb"})
        return self.outQueue.get()


    def getNumberOfStations(self):
        self.inQueue.put({"action": "getNumberOfStations"})
        return self.outQueue.get()


    def getListOfStations(self):
        self.inQueue.put({"action": "getListOfStations"})
        return self.outQueue.get()


    def getFirstStationId(self):
        self.inQueue.put({"action": "getFirstStationId"})
        return self.outQueue.get()


    def getStationById(self, stationId):
        self.inQueue.put({"action": "getStationById", "id": stationId})
        return self.outQueue.get()


    def getStationPrevious(self, stationId):
        self.inQueue.put({"action": "getStationPrevious", "id": stationId})
        return self.outQueue.get()


    def getStationNext(self, stationId):
        self.inQueue.put({"action": "getStationNext", "id": stationId})
        return self.outQueue.get()


    def updateStation(self, stationId, name, orderId, comment):
        self.inQueue.put({
            "action": "updateStation",
            "stationId": stationId,
            "name": name,
            "orderId": orderId,
            "comment": comment})
        return self.outQueue.get()


    def addStation(self, name, orderId, comment):
        self.inQueue.put({
            "action": "addStation",
            "name": name,
            "orderId": orderId,
            "comment": comment})
        return self.outQueue.get()


    def delStation(self, stationId):
        self.inQueue.put({
            "action": "delStation",
            "stationId": stationId})
        return self.outQueue.get()


    def updateUrls(self, stationId, urls):
        self.inQueue.put({
            "action": "updateUrls",
            "stationId": stationId,
            "urls": urls})
        return self. outQueue.get()
