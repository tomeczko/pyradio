#!/usr/bin/python3

import sys
import mpd
from threading import Thread
import time
import argparse

from databaseHandler import DatabaseHandler
from displayDriver import DisplayDriver
import httpServer
from pidof import pidof


def addOrUpdateDict(dictionary, key, value):
    if key in dictionary.keys() and dictionary[key] == value:
        changed = False
    else:
        changed = True
    dictionary[key] = value
    return changed


class PlayerMonitor(Thread):
    def __init__(self, player):
        Thread.__init__(self)
        self.shutdownVar = False
        self.player = player
        self.params = {}
        self.callbacks = {}
        self.internalCallbacks = {}


    def defineParamChangeCallback(self, param, callback):
        if not param in self.callbacks:
            self.callbacks[param] = []
        self.callbacks[param].append(callback)


    def defineParamChangeInternalCallback(self, param, callback):
        self.internalCallbacks[param] = callback


    def getListOfParams(self):
        return self.params.keys()


    def getParam(self, param):
        if not param in self.params:
            raise IndexError("Parameter not defined/found.")
        return self.params[param]


    def shutdown(self):
        self.shutdownVar = True


    def run(self):
        while self.shutdownVar != True:
            client = self.player.client
            try:
                statusItems = client.status()
                if not "error" in statusItems.keys():
                    statusItems["error"] = "" # Workaround to get notification when error disappears
                for key, value in statusItems.items():
                    if addOrUpdateDict(self.params, key, value):
                        if key in self.callbacks.keys() and self.callbacks[key] != None:
                            for callback in self.callbacks[key]:
                                callback(key, value)
                        if key in self.internalCallbacks.keys() and self.internalCallbacks[key] != None:
                            self.internalCallbacks[key](key, value)
                currentSongItems = client.currentsong().items()
                for key, value in currentSongItems:
                    if addOrUpdateDict(self.params, key, value):
                        if key in self.callbacks.keys() and self.callbacks[key] != None:
                            for callback in self.callbacks[key]:
                                callback(key, value)
                        if key in self.internalCallbacks.keys() and self.internalCallbacks[key] != None:
                            self.internalCallbacks[key](key, value)
                time.sleep(0.5)
            except KeyboardInterrupt:
                break
            except mpd.ConnectionError as ex:
                time.sleep(1)
                print("There was a mpd.ConnectionError exception", str(ex))
                continue
            except BrokenPipeError:
                print("BrokenPipeError!")
                time.sleep(1)
                try:
                    self.player.reconnect()
                except mpd.ConnectionError:
                    print("Already connected. What?")
                except:
                    print("Error while reconnecting from monitor thread")
                continue
        print("Shutting down the monitor thread...")


class Player():

    def __init__(self, dbFilename, portname, baudrate):
        self.initSuccessful = False
        self.currentStationDef = None
        self.expectedPlaying = False
        self.portname = portname
        self.baudrate = baudrate
        self.hostname = "localhost"
        self.port = 6600

        self.client = mpd.MPDClient(use_unicode=True)
        try:
            connectResult = self.client.connect(self.hostname, self.port)
        except ConnectionRefusedError as ex:
            print("Error connecting to mpd. Is mpd working at " + self.hostname + ":" + str(self.port) + "?")
            self.initSuccessful = False
            return
        except:
            print("Error connecting to mpd!", sys.exc_info()[0])
            self.initSuccessful = False
            return

        try:
            self.dbHandler = DatabaseHandler(dbFilename)
            self.dbHandler.start()
        except ex:
            print("Error while connecting to database.", str(ex))
            return

        if self.dbHandler.checkDb() == False:
            print("Database check failed. Check it and retry.")
            self.dbHandler.shutdown()
            return

        print("Initializing web server...")
        self.server = httpServer.HttpServer()
        self.server.player = self
        self.server.start()

        print("Initializing monitor thread...")
        self.monitor = PlayerMonitor(self)
        self.monitor.start()

        if portname is not None:
            print("Initializing display thread...")
            self.display = DisplayDriver(self, portname, baudrate)
            self.display.player = self
            self.display.start()

        firstStationId = self.dbHandler.getFirstStationId()
        if firstStationId is not None:
            self.currentStationDef = self.dbHandler.getStationById(firstStationId)

        self.initSuccessful = True


    def reconnect(self):
        print("Reconnecting with mpd...")
        time.sleep(1)
        try:
            disconnectResult = self.client.disconnect()
        except mpd.ConnectionError as ex:
            print("Disconnecting failed. Continuing...", str(ex))
        except:
            print("Disconnecting failed (unknown exception)")
        try:
            self.client = mpd.MPDClient(use_unicode=True)
            connectResult = self.client.connect(self.hostname, self.port)
        except mpd.ConnectionError:
            print("Already connected. What?")
        print("Reconnect finished")


    def shutdown(self):
        self.server.shutdown()
        self.monitor.shutdown()
        self.dbHandler.shutdown()
        if self.portname is not None:
            self.display.shutdown()


    def reboot(self):
        self.shutdown()
        from subprocess import call
        call(["reboot"])


    def getMpdPids(self):
        return pidof("mpd")


    def isInitSuccessful(self):
        return initSuccessful


    def playAddress(self, address):
        print("Playing address", address)
        result = True
        try:
            self.client.clear()
            self.client.add(address)
            self.client.play()
        except mpd.ProtocolError as err:
            print("There was a mpd.ProtocolError exception:", err)
            result = False
        except mpd.CommandError as ex:
            print("There was a command error:", ex)
            result = False
        except mpd.ConnectionError as ex:
            print("There was a mpd.ConnectionError exception", str(ex))
            result = False
        except BrokenPipeError as ex:
            print("There was a BrokenPipeError exception", str(ex))
            result = False
        except IOError as ex:
            print("There was an IOError exception", str(ex))
            result = False
        self.expectedPlaying = True
        return result


    def play(self):
        if (self.currentStationDef == None):
            raise RuntimeError("No station selected yet!")
        if (len(self.currentStationDef["urls"]) == 0):
            print(self.currentStationDef)
            raise RuntimeError("Station has no urls defined!")
        urls = self.currentStationDef["urls"]
        self.cleanupParams()
        print("Playing station", self.currentStationDef["name"], "(ID " + str(self.currentStationDef["id"]) + ")")
        # TODO BT: Add going through all urls
        result = self.playAddress(urls[0]) # Now playing just the first link
        if result == False:
            self.reconnect()
        # TODO BT: Start some kind of supervisor here


    def cleanupParams(self):
        if "name" in self.monitor.params:
            self.monitor.params.pop("name")
        if "title" in self.monitor.params:
            self.monitor.params.pop("title")
        if "bitrate" in self.monitor.params:
            self.monitor.params.pop("bitrate")


    def stop(self):
        # TODO BT: Stop supervisor here
        try:
            self.client.stop()
            self.cleanupParams()
        except mpd.ProtocolError as ex:
            print("There was a mpd.ProtocolError exception", str(ex))
        except mpd.ConnectionError as ex:
            print("There was a mpd.ConnectionError exception", str(ex))
        except BrokenPipeError as ex:
            print("There was a BrokenPipeError exception", str(ex))
        self.expectedPlaying = False


    def toggle(self):
        print("Toggling play/stop")
        if self.expectedPlaying:
            self.stop()
        else:
            self.play()


    def jumpToNext(self):
        if self.currentStationDef is not None:
            stationDef = self.dbHandler.getStationNext(self.currentStationDef["id"])
        else:
            stationDef = None
        if stationDef == None:
            raise RuntimeError("Failed jumping to next station! Probably empty database.")
        self.currentStationDef = stationDef
        if (self.expectedPlaying):
            self.play()


    def jumpToPrevious(self):
        if self.currentStationDef is not None:
            stationDef = self.dbHandler.getStationPrevious(self.currentStationDef["id"])
        else:
            stationDef = None
        if stationDef == None:
            raise RuntimeError("Failed jumping to previous station! Probably empty database.")
        self.currentStationDef = stationDef
        if (self.expectedPlaying):
            self.play()


    def jumpToStation(self, stationId):
        stationDef = self.dbHandler.getStationById(stationId)
        if stationDef == None:
            raise IndexError("No such stationId found!")
        self.currentStationDef = stationDef
        if (self.expectedPlaying):
            self.play()


    def getStationName(self):
        if self.currentStationDef == None:
            raise RuntimeError("No station selected yet!")
        return self.currentStationDef["name"]


    def getListOfStations(self):
        return self.dbHandler.getListOfStations()


    def getListOfParams(self):
        return self.monitor.getListOfParams()


    def getParam(self, param):
        return self.monitor.getParam(param)


    def defineCallback(self, event, paramName, callback):
        if event is "paramChange":
            self.monitor.defineParamChangeCallback(paramName, callback)


def callbackFunction(param, str):
    print("New value of", param, "is", str)


def showHelp():
    print("Available commands:")
    print("next - Next station")
    print("prev - Previous station")
    print("stop - Stop playing")
    print("play - Start playing")
    print("list - Show list of stations (with ids)")
    print("station n - Jump to station id=n")
    print("availParams - Show list of provided params")
    print("param x - Show value of param x (see availParams)")
    print("reconnect - Reconnect with mpd")
    print("resetmpd - Restart mpd (not implemented)")
    print("reboot - Reboot whole system")
    print("quit - Quit script")


def mainLoop(portname, baudrate):
    print("This is PyRadio...")
    player = Player("pyradio.sqlite3", portname, baudrate)
    if player.initSuccessful:
        player.defineCallback("paramChange", "state", callbackFunction)
        player.defineCallback("paramChange", "title", callbackFunction)
        player.defineCallback("paramChange", "name", callbackFunction)
        player.defineCallback("paramChange", "error", callbackFunction)
        showHelp()
        while True:
            try:
                command = input("PyRadio: ")
            except KeyboardInterrupt:
                command = "quit"
            if len(command) == 0:
                continue
            if command == "params":
                print("Params: ", player.getListOfParams())
                continue
            if command == "quit":
                player.shutdown()
                break
            if command == "reboot":
                player.reboot()
                break
            if command == "reconnect":
                player.reconnect()
                continue
            if command == "prev":
                try:
                    player.jumpToPrevious()
                except RuntimeError as ex:
                    print("Error: ", ex)
                    continue
            if command == "next":
                try:
                    player.jumpToNext()
                except RuntimeError as ex:
                    print("Error: ", ex)
                    continue
            if command == "list":
                try:
                    stations = player.getListOfStations()
                    for station in stations:
                        print("ID=" + str(station["id"]), station["name"])
                except RuntimeError as ex:
                    print("Error:", ex)
            if command == "stop":
                player.stop()
            if command == "play":
                try:
                    player.play()
                except RuntimeError as ex:
                    print("Error: ", ex)
            if command == "toggle":
                try:
                    player.toggle()
                except RuntimeError as ex:
                    print("Error: ", ex)
            if command.split()[0] == "station":
                if len(command.split()) < 2:
                    print("Please give station number as an argument")
                    continue
                try:
                    player.jumpToStation(command.split()[1])
                    print("Selected station: ", player.getStationName())
                except RuntimeError as ex:
                    print("Error: ", ex)
                except IndexError as ex:
                    print("Error: ", ex)
            if command.split()[0] == "availParams":
                print(player.getListOfParams())
            if command.split()[0] == "param":
                if len(command.split()) < 2:
                    print("Please give parameter name as an argument")
                    continue
                paramName = command.split()[1]
                try:
                    print(player.getParam(paramName))
                except IndexError as ex:
                    print("Parameter", paramName, "not defined/found!")
                except RuntimeError as ex:
                    print("Error: ", ex)

    else:
        print("Error while trying to setup the Player. Exiting...")
        exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-P", "--port", help="Serial port to be used as a display", required=False)
    parser.add_argument("-b", "--baud", help="Baudrate to be used for serial port", required=False)
    args = parser.parse_args()
    mainLoop(args.port, args.baud)


if __name__ == "__main__":
    main()
