import os

def getIp4(interface):
    try:
        ipv4 = os.popen('ip addr show ' + interface).read().split("inet ")[1].split("/")[0]
    except IndexError:
        raise RuntimeError("Error getting IP address")
    return ipv4


def getIp6(interface):
    ipv6 = os.popen('ip addr show ' + interface).read().split("inet6 ")[1].split("/")[0]
    return ipv6
