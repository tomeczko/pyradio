import http.server
import threading
from urllib.parse import urlparse, parse_qs, unquote
from cgi import parse_header, parse_multipart
import pyradio
import mpd
import datetime
import time


class Handlers(http.server.BaseHTTPRequestHandler):

    def __init__(self, request, client_address, server):
        self.currentStationName = "No station selected"
        self.servedFiles = {
            "style.css": "text/css",
            "scripts.js": "text/javascript",
            "favicon.ico": "image/x-icon",
            "pyradio.png": "image/png",
            "pyradio_256.png": "image/png",
            "play.png": "image/png",
            "play_norm.png": "image/png",
            "play_hover.png": "image/png",
            "play_push.png": "image/png",
            "stop_norm.png": "image/png",
            "stop_hover.png": "image/png",
            "stop_push.png": "image/png",
            "toggle_norm.png": "image/png",
            "toggle_hover.png": "image/png",
            "toggle_push.png": "image/png",
            "prev_norm.png": "image/png",
            "prev_hover.png": "image/png",
            "prev_push.png": "image/png",
            "next_norm.png": "image/png",
            "next_hover.png": "image/png",
            "next_push.png": "image/png",
            "edit.png": "image/png",
            "edit_norm.png": "image/png",
            "add.png" : "image/png",
            "add.png" : "image/png",
            "edit_small.png": "image/png"
        }
        try:
            http.server.BaseHTTPRequestHandler.__init__(self, request, client_address, server)
        except ConnectionResetError as ex:
            print("HttpServer: ConnectionResetError. Exception caught and handled. Continuing...")

    def _set_headers(self, returnCode = 200, contentType = 'text/html'):
        self.send_response(returnCode)
        self.send_header('Content-type', contentType)
        self.end_headers()

    def log_message(self, format, *args):
        return

    def do_GET(self):
        try:
            ctype, pdict = parse_header(self.headers['content-type'])
            if ctype == 'multipart/form-data':
                postVarsInt = parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers['content-length'])
                encodedParams = self.rfile.read(length)
                postVars = self.decodePost(encodedParams)
            else:
                postVars = None
        except TypeError as ex:
            postVars = None
            pass # There is no problem - just not a POST call

        path = self.path.strip("/")
        requestQuery = urlparse(path).query
        action = parse_qs(requestQuery).get("action", None)
        stationId = parse_qs(requestQuery).get("stationId", None)
        params = {}
        if action is not None:
            action = action[0]
        if stationId is not None:
            params["stationId"] = stationId[0]

        if postVars is not None:
            if postVars['postAction'] == "editStationPost":
                self.editStationPost(postVars)
            elif postVars['postAction'] == "addStationPost":
                self.addStationPost(postVars)
            else:
                print("postAction", postVars['postAction'], "was not recognised!")

        if action == "play" or action == "stop" or action == "next" or action == "prev" or action == "toggle":
            result = self.processAction(action, params)
        elif action == "editStation" or action == "addStation":
            result = self.constructOutput(action, params)
        elif path == "getStatus":
            result = self.constructStatus()
        elif path in self.servedFiles:
            result = self.serveFile(path)
        elif action == "delStation":
            print("Deleting station", params["stationId"])
            self.delStationPost(params["stationId"])
            result = self.constructOutput(action, params)
        elif path == "":
            result = self.constructOutput(action, params)
        else:
            result = self.constructNotFoundError(path)
        try:
            self.wfile.write(result)
        except BrokenPipeError:
            print("Got BrokenPipeError while processing action", action)

    def do_POST(self):
        self.do_GET()

    def constructOutput(self, action=None, params=None):
        self._set_headers()
        result = "<html>\n"
        result += "<head>\n"
        result += "<title>PyRadio</title>\n"
        result += "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>\n"
        result += "<script src=\"scripts.js\"></script>"
        result += "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />\n"
        result += "<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\n"
        result += "</head>"
        result += "<body onLoad=\"startRefreshLoop();\">"
        result += "<h1>PyRadio - Python-based internet radio player</h1>"
        result += self.printMenu()
        result += "<br />\n"
        result += "<div id=\"statusDiv\" class=\"statusDiv\">(status)</div>\n"
        result += "<div id=\"resultDiv\" class=\"resultDiv\">(result)</div>\n"
        result += "<br />\n"
        if action is None or action == "mainpage":
            result += self.printMainpage()
        elif action == "urls":
            result += self.printUrls(params["stationId"])
        elif action == "addStation":
            result += self.addEditStation(None)
        elif action == "delStation":
            result += self.printMainpage()
        elif action == "editStation":
            result += self.addEditStation(params["stationId"])
        elif action == "stationOrderInc":
            result += action + " is not yet implemented!<br />"
            result += self.printMainpage()
        elif action == "stationOrderDec":
            result += action + " is not yet implemented!<br />"
            result += self.printMainpage()
        else:
            result += action + " is an unknown function or not yet implemented!"
        result += "</body>"
        result += "</html>"
        return result.encode("utf-8")

    def processAction(self, action, params):
        result = " "
        if action == "prev":
            try:
                self.server.player.jumpToPrevious()
            except RuntimeError as ex:
                result += str(ex) + "<br />\n"
            except mpd.ConnectionError as ex:
                result += "mpd connection error: " + str(ex) + "<br />\n"
        elif action == "next":
            try:
                self.server.player.jumpToNext()
            except RuntimeError as ex:
                result += str(ex) + "<br />\n"
            except mpd.ConnectionError as ex:
                result += "mpd connection error: " + str(ex) + "<br />\n"
        elif action == "play":
            try:
                if "stationId" in params:
                    self.server.player.jumpToStation(int(params["stationId"]))
                self.server.player.play()
            except RuntimeError as ex:
                result += str(ex) + "<br />\n"
            except mpd.ConnectionError as ex:
                result += "mpd connection error: " + str(ex) + "<br />\n"
        elif action == "stop":
            try:
                self.server.player.stop()
            except RuntimeError as ex:
                result += str(ex) + "<br />\n"
            except mpd.ConnectionError as ex:
                result += "mpd connection error: " + str(ex) + "<br />\n"
        elif action == "toggle":
            try:
                self.server.player.toggle()
            except RuntimeError as ex:
                result += str(ex) + "<br />\n"
            except mpd.ConnectionError as ex:
                result += "mpd connection error: " + str(ex) + "<br />\n"
        else:
            result += action + " is an unknown function or not yet implemented!"
        return result.encode("utf-8")

    def constructStatus(self):
        self._set_headers()
        result = ""
        result += time.strftime("%a, %H:%M:%S") + " "
        mpdPids = self.server.player.getMpdPids()
        if len(mpdPids):
            result += "<br />\n";
        else:
            result += "<span class=\"warningSpan\">mpd not running!</span><br/>\n"
        if self.server.player.currentStationDef is not None:
            result += "Station: " + self.server.player.currentStationDef["name"] + "<br />\n"
        else:
            result += "Station: " + "no stations definions in database yet!" + "<br />\n"
        if "state" in self.server.player.monitor.params:
            state = self.server.player.monitor.params["state"]
            result += "State: " + state
            if state == "play":
                try:
                    elapsed = self.server.player.monitor.params["elapsed"]
                    elapsed = datetime.timedelta(seconds=round(float(elapsed)))
                    result += " (" + str(elapsed) + ")"
                except KeyError:
                    result += " (?)"
            result += "<br />\n"
        currentParams = self.server.player.monitor.params
        if "state" in currentParams and currentParams["state"] == "play":
            result += "Currently playing: "
            if "name" in currentParams:
                result += currentParams["name"]
            if "title" in currentParams:
                result += ": " + currentParams["title"]
            result += "<br />\n"
            if "bitrate" in currentParams:
                result += "Bitrate: " + currentParams["bitrate"] + " kbps" + "<br />\n"
            else:
                result += "Bitrate unknown" + "<br />\n"
        errorString = self.server.player.monitor.params["error"]
        result += "<script>updateResult('" + errorString + "');</script>\n"
        return result.encode("utf-8")

    def constructNotFoundError(self, path):
        self._set_headers(404)
        result = "<html><body><h2>404: Not found</h2><h4>Path: " + path + "</h4></body></html>\n"
        return result.encode("utf-8")

    def serveFile(self, filename):
        self._set_headers(200, self.servedFiles[filename])
        result = bytes()
        try:
            source = open(filename, "rb")
            while True:
                buf = source.read(1024)
                if len(buf) == 0:
                    break
                result += buf
        except IOError as ex:
            print("Error:", ex)
            result = "".encode("utf-8")
        return result

    def printMenu(self):
        def addButton(onClick, imageNorm, imageHover, imagePush):
            tag = "<img src=\"" + imageNorm + "\" "
            tag += "onClick=\"" + onClick + "\" "
            tag += "onMouseOver=\"this.src='" + imageHover + "'\" "
            tag += "onMouseOut=\"this.src='" + imageNorm + "'\" "
            tag += "onMouseDown=\"this.src='" + imagePush + "'\" "
            tag += "onMouseUp=\"this.src='" + imageHover + "'\" />"
            return tag

        result = "<div class=\"menuDiv\">\n"
        result += addButton("makeAction('toggle')", "toggle_norm.png", "toggle_hover.png", "toggle_push.png")
        result += addButton("makeAction('prev')", "prev_norm.png", "prev_hover.png", "prev_push.png")
        result += addButton("makeAction('play')", "play_norm.png", "play_hover.png", "play_push.png")
        result += addButton("makeAction('stop')", "stop_norm.png", "stop_hover.png", "stop_push.png")
        result += addButton("makeAction('next')", "next_norm.png", "next_hover.png", "next_push.png")
        result += "</div>\n"
        return result

    def printMainpage(self):
        result = "There are "
        result += str(self.server.player.dbHandler.getNumberOfStations())
        result += " stations in the database (click to play).<br />\n"
        stations = self.server.player.dbHandler.getListOfStations()
        result += "<table>\n"
        for station in stations:
            result += "<tr>"
            result += "<td><a href=\"?action=editStation&stationId=" + str(station["id"]) + "\"><img src=\"edit_small.png\" /></a></td>"
            link = "?action=play&stationId=" + str(station["id"])
            result += "<td><a onClick=\"makeActionPlayStation('" + str(station["id"]) + "')\">" + station["name"]
            result += "</a></td>\n"
            result += "<td>" + str(station["orderId"]) + "</td>\n"
            if station["comment"] is not None:
                result += "<td>" + station["comment"] + "</td>\n"
            else:
                result += "<td></td>"
            result += "</tr>\n"
        result += "</table>\n"
        result += "<a href=\"?action=addStation\">Add new station</a><br />\n"
        return result

    def addEditStation(self, stationId):
        result = ""
        if stationId is not None:
            currentStationDetails = self.server.player.dbHandler.getStationById(stationId)
        else:
            currentStationDetails = None
        if stationId is not None and currentStationDetails is None:
            result += "Error getting station details!<br />\n"
            return result
        result += "<form method=\"post\" action=\"/\">\n"
        if stationId is not None:
            result += "<input type=\"hidden\" name=\"postAction\" value=\"editStationPost\" />\n"
        else:
            result += "<input type=\"hidden\" name=\"postAction\" value=\"addStationPost\" />\n"
        if stationId is not None:
            result += "<input type=\"hidden\" name=\"stationId\" value=\"" + str(currentStationDetails["id"]) + "\" />\n"
        result += "<table class=\"editStation\">\n"
        result += "<tr><td>Station name</td><td><input type=\"text\" name=\"name\" size=\"60\" value=\""
        if stationId is not None:
            result += currentStationDetails["name"]
        result += "\"/></td></tr>\n"
        result += "<tr><td>order id</td><td><input type=\"text\" name=\"orderId\" size=\"5\" value=\""
        if stationId is not None:
            result += str(currentStationDetails["orderId"])
        else:
            result += "0"
        result += "\"/></td></tr>\n"
        result += "<tr><td>comment</td><td><input type=\"text\" name=\"comment\" size=\"60\" value=\""
        if stationId is not None and currentStationDetails["comment"] is not None:
            result += currentStationDetails["comment"]
        result += "\"/></td></tr>\n"
        result += "<tr><td>URLs</td><td><textarea name=\"urls\" cols=\"60\" rows=\"4\">"
        if stationId is not None:
            for url in currentStationDetails["urls"]:
                result += url + "\n"
        result += "</textarea></td></tr>\n"
        result += "<tr><td></td><td><input type=\"submit\" value=\"Save\" /></td></tr>\n"
        if stationId is not None:
            deleteString = "?action=delStation&stationId=" + str(stationId)
            result += "<tr><td></td><td><button type=\"button\" onClick=\"window.location.href = '" + deleteString + "';\">Delete station</button></td></tr>\n"
        result += "</table>\n"
        result += "</form>\n"
        return result

    def editStationPost(self, postVars):
        try:
            print("postVars", postVars)
            self.server.player.dbHandler.updateStation(
                stationId = postVars["stationId"],
                name = postVars["name"],
                orderId = int(postVars["orderId"]),
                comment = postVars["comment"]
            )
            urls = postVars["urls"].split()
            self.server.player.dbHandler.updateUrls(
                stationId = postVars["stationId"],
                urls = urls)
        except RuntimeError as ex:
            print("Editing station failed:", str(ex))

    def addStationPost(self, postVars):
        try:
            newId = self.server.player.dbHandler.addStation(
                name = postVars["name"],
                orderId = int(postVars["orderId"]),
                comment = postVars["comment"]
            )
            if type(newId) is type(RuntimeError()):
                raise newId
            print("ID of just added station is", newId)
            self.newId = newId
            urls = postVars["urls"].split()
            self.server.player.dbHandler.updateUrls(
                stationId = newId,
                urls = urls)
        except ValueError as ex:
            print("Encountered a value error:", str(ex))
        except RuntimeError as ex:
            print("Adding station failed:", str(ex))

    def delStationPost(self, stationId):
        try:
            self.server.player.dbHandler.delStation(stationId)
        except ValueError as ex:
            print("Encountered a value error:", str(ex))


    def printUrls(self, stationId):
        result = "<h2>List of urls for station XXX (id=" + str(stationId) + ")</h2><br />"
        result += "To be implemented!"
        return result


    def decodePost(self, postContent):
        spaced = postContent.split(b'&')
        output = {}
        for item in spaced:
            kvPairs = item.split(b'=')
            key = kvPairs[0].decode()
            value = kvPairs[1].decode()
            value = value.replace('+', ' ')
            value = unquote(value)
            output[key] = value
        return output


class HttpServer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.dbHandler = None

    def run(self):
        server_address_root = ('', 80)
        server_address_user = ('', 8080)
        handlers = Handlers
        try:
            print("Starting http server on port", server_address_root[1], "...")
            self.httpd = http.server.HTTPServer(server_address_root, handlers)
        except PermissionError:
            print("Trying port", server_address_user[1], "for http server...")
            self.httpd = http.server.HTTPServer(server_address_user, handlers)
        if self.player is None:
            raise RuntimeError("player variable must be set before starting server thread!")
        self.httpd.player = self.player
        print('Http server started...')
        self.httpd.serve_forever()
        print("Shutting down the http server...")

    def shutdown(self):
        self.httpd.shutdown()



