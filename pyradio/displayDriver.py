import datetime
import queue
import serial
import time
import threading
import ipProvider


SERIAL_TIMEOUT = 0

BACKLIGHT_WORK = 1000
BACKLIGHT_IDLE = 200
BACKLIGHT_TIMEOUT = 600
DISPLAY_PORCH = 7
DISPLAY_SCROLL_DELAY = 17
DISPLAY_CYCLE_TIME = 4
DISPLAY_CYCLE_MAX = 2
LCD_WIDTH = 16
INTERFACE = "eth0"


class TimeProvider(threading.Thread):
    def __init__(self, destinationQueue):
        threading.Thread.__init__(self)
        self.shutdownVar = False
        self.destinationQueue = destinationQueue

    def run(self):
        while self.shutdownVar == False:
            currentTime = time.strftime("%a, %H:%M:%S")
            self.destinationQueue.put({"param": "time", "value": currentTime})
            time.sleep(1.0)
        print("Shutting down TimeProvider thread...")


class DisplayDriver(threading.Thread):
    def __init__(self, player, portname, baudrate):
        threading.Thread.__init__(self)
        self.shutdownVar = False
        self.player = player
        self.portname = portname
        self.baudrate = baudrate
        self.changesQueue = queue.Queue()
        self.timeProvider = TimeProvider(self.changesQueue)
        self.timeProvider.start()

        # Here we define what we would like to be informed about
        self.player.monitor.defineParamChangeCallback("state", self.paramChangeCallback)

    def resetDisplayCycle(self):
        self.displayCycleReloadTime = datetime.datetime.now() + datetime.timedelta(seconds=DISPLAY_CYCLE_TIME)
        self.displayCycle = 0
        print("displayCounter reset")

    def paramChangeCallback(self, param, value):
        self.changesQueue.put({"param": param, "value": value})

    def processLine(self, line):
        if self.inIdle:
            self.updateDimTime()
            return

        try:
            buttonNumber = int(line.split()[1])
            buttonState = line.split()[2]
        except IndexError:
            return
        except ValueError:
            return
        if buttonState == "short":
            if buttonNumber == 1:
                if self.currentMenu == "volume":
                    print("Dec volume")
                if self.currentMenu == "station":
                    try:
                        self.player.jumpToNext()
                        self.resetDisplayCycle()
                    except RuntimeError:
                        pass
                    self.resetScroll()
                if self.currentMenu == "restartMpd":
                    print("Restarting mpd (not implemented)")
                if self.currentMenu == "reboot":
                    print("Rebooting pyradio")
                    self.currentMenu = "rebooting"
                    self.player.reboot()
                    print("Shutdown...")
            if buttonNumber == 2:
                if self.currentMenu == "volume":
                    print("Inc volume")
                if self.currentMenu == "station":
                    try:
                        self.player.jumpToPrevious()
                        self.resetDisplayCycle()
                    except RuntimeError:
                        pass
                    self.resetScroll()
                if self.currentMenu == "restartMpd":
                    self.currentMenu = "station"
                if self.currentMenu == "reboot":
                    self.currentMenu = "station"
            if buttonNumber == 3:
                self.player.toggle()
        if buttonState == "middle":
            if buttonNumber == 1:
                print("Switching to volume control")
                self.currentMenu = "volume"
            if buttonNumber == 2:
                print("Switching to choise of channel")
                self.currentMenu = "station"
            if buttonNumber == 3:
                print("Showing menu \"restartMpd\"")
                self.currentMenu = "restartMpd"
        if buttonState == "long":
            if buttonNumber == 1:
                print("Long press of button 1 (not yet implemented)")
            if buttonNumber == 2:
                print("Long press of button 2 (not yet implemented)")
            if buttonNumber == 3:
                print("Showing menu \"reboot\"")
                self.currentMenu = "reboot"
        self.updateLcd()
        self.updateDimTime()

    def updateDimTime(self):
        self.dimTime = datetime.datetime.now() + datetime.timedelta(seconds=BACKLIGHT_TIMEOUT)
        self.lcdBacklight(BACKLIGHT_WORK)
        self.inIdle = False


    def lcdSendBytes(self, string):
        self.serialPort.write(string.encode("utf-8"))

    def lcdBacklight(self, backlight):
        self.lcdSendBytes("backlight " + str(backlight) + "\n")

    def initLcd(self):
        self.lcdBacklight(BACKLIGHT_WORK)
        self.lcdSendBytes("lcdwidth " + str(LCD_WIDTH) + "\n")
        self.lcdSendBytes("scrollporch " + str(DISPLAY_PORCH) + "\n")
        self.lcdSendBytes("scrolldelay " + str(DISPLAY_SCROLL_DELAY) + "\n")
        self.lcdSendBytes("settext 1  \n")
        self.lcdSendBytes("settext 2  \n")

    def deinitLcd(self):
        self.lcdBacklight(BACKLIGHT_IDLE)

    def resetScroll(self):
        self.lcdSendBytes("resetscroll 1\n")
        self.lcdSendBytes("resetscroll 2\n")

    def updateLcd(self):
        playerParams = self.player.monitor.params
        stationDef = self.player.currentStationDef
        name = playerParams["name"] if "name" in playerParams else ""
        title = playerParams["title"] if "title" in playerParams else ""
        bitrate = playerParams["bitrate"] if "bitrate" in playerParams else "?"
        bitrate += " kbps"
        time = playerParams["time"] if "time" in playerParams else "0"
        try:
            elapsed = playerParams["elapsed"]
            elapsed = datetime.timedelta(seconds=round(float(elapsed)))
            timeStr = str(elapsed)
        except:
            timeStr = "?"
        try:
            stationName = stationDef["name"] if "name" in stationDef else "Station unnamed"
        except TypeError as ex:
            stationName = "?"
        try:
            if self.currentMenu == "station":
                if "state" in playerParams and playerParams["state"] == "play":
                    if len(title) > 0:
                        self.lcdSendBytes("settext 1 " + stationName + ": " + name + ", " + title + "\n")
                    else:
                        self.lcdSendBytes("settext 1 " + stationName + ": " + name + "\n")
                    self.lcdSendBytes("scroll 1 1\n")
                    if self.displayCycle == 0:
                        self.lcdSendBytes("settext 2 " + "playing " + bitrate + "\n")
                        self.lcdSendBytes("scroll 2 0\n")
                    elif self.displayCycle == 1:
                        text = "playing " + timeStr
                        self.lcdSendBytes("settext 2 " + text + "\n")
                        if len(text) > LCD_WIDTH:
                            self.lcdSendBytes("scroll 2 2\n")
                        else:
                            self.lcdSendBytes("scroll 2 0\n")
                    elif self.displayCycle == 2:
                        try:
                            self.lcdSendBytes("settext 2 " + ipProvider.getIp4(INTERFACE) + "\n")
                        except RuntimeError:
                            self.lcdSendBytes("settext 2    IP unknown" + "\n")
                else:
                    self.lcdSendBytes("settext 1  " + self.lcdValues["time"] + "\n")
                    errorString = playerParams["error"] if "error" in playerParams else ""
                    if not self.inIdle:
                        if len(errorString) > 0:
                            self.lcdSendBytes("settext 2 " + errorString + "\n")
                            if len(errorString) > LCD_WIDTH:
                                self.lcdSendBytes("scroll 2 2\n")
                            else:
                                self.lcdSendBytes("scroll 2 0\n")
                        else:
                            if self.displayCycle == 0 or self.displayCycle == 1:
                                self.lcdSendBytes("settext 2 " + stationName + "\n")
                                if len(stationName) > LCD_WIDTH:
                                    self.lcdSendBytes("scroll 2 2\n")
                                else:
                                    self.lcdSendBytes("scroll 2 0\n")
                            else:
                                try:
                                    self.lcdSendBytes("settext 2 " + ipProvider.getIp4(INTERFACE) + "\n")
                                except RuntimeError:
                                    self.lcdSendBytes("settext 2    IP unknown" + "\n")
                                self.lcdSendBytes("scroll 2 0\n")
                    else:
                        try:
                            self.lcdSendBytes("settext 2 " + ipProvider.getIp4(INTERFACE) + "\n")
                        except RuntimeError:
                            self.lcdSendBytes("settext 2    IP unknown" + "\n")
                    self.lcdSendBytes("scroll 1 0\n")
            if self.currentMenu == "volume":
                self.lcdSendBytes("settext 1 Volume:\n")
                self.lcdSendBytes("settext 2 not yet impl.\n")
                self.lcdSendBytes("scroll 1 0\n")
                self.lcdSendBytes("scroll 2 0\n")
            if self.currentMenu == "restartMpd":
                self.lcdSendBytes("settext 1  Restart mpd?  \n")
                self.lcdSendBytes("settext 2  YES    CANCEL \n")
                self.lcdSendBytes("scroll 1 0\n")
                self.lcdSendBytes("scroll 2 0\n")
                self.resetScroll()
            if self.currentMenu == "reboot":
                self.lcdSendBytes("settext 1 Reboot pyradio?\n")
                self.lcdSendBytes("settext 2  YES    CANCEL \n")
                self.lcdSendBytes("scroll 1 0\n")
                self.lcdSendBytes("scroll 2 0\n")
                self.resetScroll()
            if self.currentMenu == "rebooting":
                self.lcdSendBytes("settext 1 Rebooting...\n")
                self.lcdSendBytes("settext 2  \n")
                self.lcdSendBytes("scroll 1 0\n")
                self.lcdSendBytes("scroll 2 0\n")
                self.resetScroll()

        except ValueError:
            print(self.threadName + ": ValueError")
        except KeyError as ex:
            print(self.threadName + ": KeyError", str(ex))
        self.serialPort.flushOutput()

    def run(self):
        self.threadName = "Display thread"
        self.inIdle = False
        self.currentMenu = "station"
        self.displayCycle = 0
        self.displayCycleReloadTime = datetime.datetime.now()
        if self.player is None:
            raise RuntimeError("player variable must be set before starting server thread!")
        print("Initialization of display...")
        self.lcdValues = {}
        self.lcdValues["time"] = "Time unknown"
        try:
            self.serialPort = serial.Serial(self.portname, self.baudrate, timeout=SERIAL_TIMEOUT, xonxoff=0, rtscts=0)
            self.initLcd()
        except FileNotFoundError as ex:
            print("Error opening serial port!", str(ex))
            self.shutdownVar = True
            return
        except serial.serialutil.SerialException as ex:
            print("Error opening serial port!", str(ex))
            self.shutdownVar = True
            return
        self.updateDimTime()
        while self.shutdownVar == False:
            lines = self.serialPort.readlines()
            for line in lines:
                line = line.decode("utf-8")
                self.processLine(line)
                print("End of line processing. shutdown is", self.shutdownVar)
            try:
                msg = self.changesQueue.get(timeout=0.10)
            except queue.Empty:
                msg = None
                pass
            if msg != None:
                # Uncomment this line for debugging. For real display do some thingies to send data to display instead.
                # print(self.threadName + ": ", msg["param"], "is now", msg["value"])
                self.lcdValues[msg["param"]] = msg["value"]
            self.updateLcd()
            if datetime.datetime.now() > self.dimTime and self.inIdle == False:
                self.lcdBacklight(BACKLIGHT_IDLE)
                self.inIdle = True
            if datetime.datetime.now() > self.displayCycleReloadTime:
                self.displayCycleReloadTime = datetime.datetime.now() + datetime.timedelta(seconds=DISPLAY_CYCLE_TIME)
                self.displayCycle = self.displayCycle + 1
                if self.displayCycle > DISPLAY_CYCLE_MAX:
                    self.displayCycle = 0

        self.timeProvider.shutdownVar = True
        self.deinitLcd()
        self.serialPort.flushOutput()
        self.serialPort.flush()
        time.sleep(0.1)
        self.serialPort.close()
        print("Shutting down the display thread...")

    def shutdown(self):
        self.shutdownVar = True
